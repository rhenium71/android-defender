package com.example.hungers.androiddefender;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class image_viewer extends Activity {
    public static String PREFS_NAME="hungers";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams windowManager=getWindow().getAttributes();
        windowManager.dimAmount=0.80f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_image_viewer);

        ImageView iv = (ImageView) findViewById(R.id.iv);
        FrameLayout fl = (FrameLayout) findViewById(R.id.fl);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        if(preference.getString("iv","").equals("1")){
            iv.setImageResource(R.drawable.raffi);
        }
        else if(preference.getString("iv","").equals("2")){
            iv.setImageResource(R.drawable.nayeem);
        }
        else if(preference.getString("iv","").equals("3")){
            iv.setImageResource(R.drawable.emroze);
        }
        else if(preference.getString("iv","").equals("4")){
            iv.setImageResource(R.drawable.samu);
        }


        fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }
}
