package com.example.hungers.androiddefender;

/**
 * Created by emroz on 12/17/2016.
 */
import android.graphics.Color;

import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;

/**
 * Created by Weiping Huang at 23:44 on 16/11/21
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */
public class BuilderManager {

    private static int[] color = new int[]{
            R.color.color1,
            R.color.color2,
            R.color.color3,
            R.color.color4,
            R.color.color5,
            R.color.color6,
    };

    //flotting window
    private static int[] imageResourcesfloating = new int[]{
            R.drawable.scr_lock,
            R.drawable.track_loc,
            R.drawable.setti,
            R.drawable.my_loc,
    };
    private static int[] txtResourcesfloating = new int[]{
            R.string.one5,
            R.string.two5,
            R.string.three5,
            R.string.four5,
    };
    private static int[] txtResourcesSubfloating = new int[]{
            R.string.sub_one5,
            R.string.sub_two5,
            R.string.sub_three5,
            R.string.sub_four5,
    };

    //track
    private static int[] imageResourcestrack = new int[]{
            R.drawable.track_loc,
            R.drawable.abo_track,
            R.drawable.show_track,
            R.drawable.my_loc,
    };
    private static int[] txtResourcestrack = new int[]{
            R.string.one1,
            R.string.two1,
            R.string.three1,
            R.string.four1,
    };
    private static int[] txtResourcesSubtrack = new int[]{
            R.string.sub_one1,
            R.string.sub_two1,
            R.string.sub_three1,
            R.string.sub_four1,
    };
    //security
    private static int[] imageResourcesSecurity = new int[]{
            R.drawable.find_my_phone,
            R.drawable.lock,
            R.drawable.reset,
            R.drawable.wipe_data,
            R.drawable.disable_camera,
            R.drawable.enable_camera,
    };
    private static int[] txtResources_Security = new int[]{
            R.string.one2,
            R.string.two2,
            R.string.three2,
            R.string.four2,
            R.string.five2,
            R.string.six2,
    };
    //others
    private static int[] imageResourcesothers = new int[]{

            R.drawable.stay_close,

            R.drawable.setti,
            R.drawable.photo_loc,
            R.drawable.setti,
    };
    private static int[] txtResourcesothers = new int[]{

            R.string.two3,

            R.string.four3,
            R.string.five3,
            R.string.six3,
    };
    private static int[] txtResourcesSubothers = new int[]{

            R.string.sub_two3,

            R.string.sub_four3,
            R.string.sub_five3,
            R.string.sub_six3,
    };
    //settings
    private static int[] imageResources1 = new int[]{
            R.drawable.setti,
            R.drawable.setti,
            R.drawable.setti,
            R.drawable.setti,
            R.drawable.setti,

    };
    private static int[] txtResources = new int[]{
            R.string.one,
            R.string.two,
            R.string.three,
            R.string.four,
            R.string.five,


    };
    private static int[] txtResourcesSub = new int[]{
            R.string.sub_one,
            R.string.sub_two,
            R.string.sub_three,
            R.string.sub_four,
            R.string.sub_five,

    };
    //about
    private static int[] imageResourcesabout = new int[]{
            R.drawable.ab_about,
            R.drawable.help,
            R.drawable.rate,
            R.drawable.feedback,
    };
    private static int[] txtResourcesabout = new int[]{
            R.string.one4,
            R.string.two4,
            R.string.three4,
            R.string.four4,
    };





    private static int color_index = 0;

//floating window
    private static int imageResourceIndexfloating = 0;
    private static int txtResourceIndexsubfloating = 0;
    private static int txtResourceIndexfloating = 0;
//track
    private static int imageResourceIndextrack = 0;
    private static int txtResourceIndexsubtrack = 0;
    private static int txtResourceIndextrack = 0;
//security
    private static int imageResourceIndex = 0;
    private static int txtResourceIndexSecurity = 0;
//others
    private static int imageResourceIndexothers = 0;
    private static int txtResourceIndexothers = 0;
    private static int txtResourceIndexSubothers = 0;
//security
    private static int imageResourceIndexabout = 0;
    private static int txtResourceIndexabout = 0;
//settings
    private static int imageResourceIndex1 = 0;
    private static int txtResourceIndex = 0;
    private static int txtResourceIndexSub = 0;


    static int getcolor() {
        if (color_index >= color.length) color_index = 0;
        return color[color_index++];
    }
    //floatinf window
    static int getImageResourcefloating() {
        if (imageResourceIndexfloating >= imageResourcesfloating.length) imageResourceIndexfloating = 0;
        return imageResourcesfloating[imageResourceIndexfloating++];
    }
    static int getTxtResourcefloating() {
        if (txtResourceIndexfloating >= txtResourcesfloating.length) txtResourceIndexfloating = 0;
        return txtResourcesfloating[txtResourceIndexfloating++];
    }
    static int getTxtResourceSubfloating() {
        if (txtResourceIndexsubfloating >= txtResourcesSubfloating.length) txtResourceIndexsubfloating = 0;
        return txtResourcesSubfloating[txtResourceIndexsubfloating++];
    }
    //track
    static int getImageResourcetrack() {
        if (imageResourceIndextrack >= imageResourcestrack.length) imageResourceIndextrack = 0;
        return imageResourcestrack[imageResourceIndextrack++];
    }
    static int getTxtResourcetrack() {
        if (txtResourceIndextrack >= txtResourcestrack.length) txtResourceIndextrack = 0;
        return txtResourcestrack[txtResourceIndextrack++];
    }
    static int getTxtResourceSubtrack() {
        if (txtResourceIndexsubtrack >= txtResourcesSubtrack.length) txtResourceIndexsubtrack = 0;
        return txtResourcesSubtrack[txtResourceIndexsubtrack++];
    }
    //security
    static int getImageResource() {
        if (imageResourceIndex >= imageResourcesSecurity.length) imageResourceIndex = 0;
        return imageResourcesSecurity[imageResourceIndex++];
    }
    static int getTxtResourceSecurity() {
        if (txtResourceIndexSecurity >= txtResources_Security.length) txtResourceIndexSecurity = 0;
        return txtResources_Security[txtResourceIndexSecurity++];
    }
    //others

    static int getImageResourceothers() {
        if (imageResourceIndexothers >= imageResourcesothers.length) imageResourceIndexothers = 0;
        return imageResourcesothers[imageResourceIndexothers++];
    }
    static int getTxtResourceothers() {
        if (txtResourceIndexothers >= txtResourcesothers.length) txtResourceIndexothers = 0;
        return txtResourcesothers[txtResourceIndexothers++];
    }
    static int getTxtResourceSubothers() {
        if (txtResourceIndexSubothers >= txtResourcesSubothers.length) txtResourceIndexSubothers = 0;
        return txtResourcesSubothers[txtResourceIndexSubothers++];
    }
    //about
    static int getImageResourceabout() {
        if (imageResourceIndexabout >= imageResourcesabout.length) imageResourceIndexabout = 0;
        return imageResourcesabout[imageResourceIndexabout++];
    }
    static int getTxtResourceabout() {
        if (txtResourceIndexabout >= txtResourcesabout.length) txtResourceIndexabout = 0;
        return txtResourcesabout[txtResourceIndexabout++];
    }
    //settings

    static int getImageResource1() {
        if (imageResourceIndex1 >= imageResources1.length) imageResourceIndex1 = 0;
        return imageResources1[imageResourceIndex1++];
    }

    static int getTxtResource() {
        if (txtResourceIndex >= txtResources.length) txtResourceIndex = 0;
        return txtResources[txtResourceIndex++];
    }
    static int getTxtResourceSub() {
        if (txtResourceIndexSub >= txtResourcesSub.length) txtResourceIndexSub = 0;
        return txtResourcesSub[txtResourceIndexSub++];
    }




    static SimpleCircleButton.Builder getSimpleCircleButtonBuilder() {
        return new SimpleCircleButton.Builder()
                .normalImageRes(getImageResource());
    }

    static TextInsideCircleButton.Builder getTextInsideCircleButtonBuilder() {
        return new TextInsideCircleButton.Builder()
                .normalImageRes(getImageResource())
                .textSize(13)
                .normalTextRes(getTxtResourceSecurity())
                .pieceColor(Color.WHITE);
    }

    static TextInsideCircleButton.Builder getTextInsideCircleButtonBuilderWithDifferentPieceColor() {
        return new TextInsideCircleButton.Builder()
                .normalImageRes(getImageResourceabout())
                .textSize(15)
                .normalTextRes(getTxtResourceabout())
                .pieceColor(Color.WHITE);
    }

    static TextOutsideCircleButton.Builder getTextOutsideCircleButtonBuilder() {
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(getImageResource())
                .normalTextRes(R.string.text_outside_circle_button_text_normal);
    }

    static TextOutsideCircleButton.Builder getTextOutsideCircleButtonBuilderWithDifferentPieceColor() {
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(getImageResource())
                .normalTextRes(R.string.text_outside_circle_button_text_normal)
                .pieceColor(Color.WHITE);
    }
//settings
    static HamButton.Builder getHamButtonBuilder() {
        return new HamButton.Builder()
                .normalImageRes(getImageResource1())
                .normalTextRes(getTxtResource())
                .textSize(18)
                .subTextSize(13)
                .subNormalTextColor(Color.parseColor("#ffffff"))
                .normalTextColor(Color.parseColor("#86e3f4"))
                .subNormalTextRes(getTxtResourceSub())
                .normalColor(Color.parseColor("#00306E"))
                .pieceColor(Color.WHITE);
    }
//floating window
    static HamButton.Builder getHamButtonBuilder2() {
        return new HamButton.Builder()
                .normalImageRes(getImageResourcefloating())
                .normalTextRes(getTxtResourcefloating())
                .rippleEffect(true)
                .textSize(18)
                .subTextSize(13)
                .normalTextColor(Color.parseColor("#86e3f4"))
                .shadowColor(Color.parseColor("#CC00306E"))
                .subNormalTextRes(getTxtResourceSubfloating())
                .normalColor(Color.parseColor("#00306E"))
                .pieceColor(Color.WHITE);
    }
//track
    static HamButton.Builder getHamButtonBuilder0() {
        return new HamButton.Builder()
                .normalImageRes(getImageResourcetrack())
                .normalTextRes(getTxtResourcetrack())
                .rippleEffect(true)
                .textSize(18)
                .subTextSize(13)
                .normalTextColor(Color.parseColor("#86e3f4"))
                .shadowColor(Color.parseColor("#CC00306E"))
                .subNormalTextRes(getTxtResourceSubtrack())
                .normalColor(Color.parseColor("#00306E"))
                .pieceColor(Color.WHITE);
    }
//others
    static HamButton.Builder getHamButtonBuilder1() {
        return new HamButton.Builder()
                .normalImageRes(getImageResourceothers())
                .normalTextRes(getTxtResourceothers())
                .textSize(18)
                .subTextSize(13)
                .normalTextColor(Color.parseColor("#86e3f4"))
                .subNormalTextRes(getTxtResourceSubothers())
                .normalColor(Color.parseColor("#00306E"))
                .pieceColor(Color.WHITE);
    }

    private static BuilderManager ourInstance = new BuilderManager();

    public static BuilderManager getInstance() {
        return ourInstance;
    }

    private BuilderManager() {
    }
}
