package com.example.hungers.androiddefender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class how_work extends Activity {


    public static String PREFS_NAME="hungers";
    TextView tv,tv1,tv2;
    FrameLayout ll;
    Button b1,b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams windowManager=getWindow().getAttributes();
        windowManager.dimAmount=0.60f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_how_work);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();



        tv=(TextView) findViewById(R.id.tv1);
        tv1=(TextView) findViewById(R.id.tv_rs);
        tv2=(TextView) findViewById(R.id.tv_rp);


        b2=(Button) findViewById(R.id.btn_star);

        ll =(FrameLayout) findViewById(R.id.fl1);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        tv.setTypeface(type);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");

        b2.setTypeface(type1);
        tv1.setTypeface(type1);
        tv2.setTypeface(type1);

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b2.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b2.getBackground().setAlpha(255);

                        finish();

                        break;
                }
                return false;
            }
        });

    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


    }
}
