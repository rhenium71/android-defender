package com.example.hungers.androiddefender;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class admin_lock extends AppCompatActivity {
    public static String PREFS_NAME = "hungers";

    Button ok;
    EditText et_input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_lock);
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();


        // Toast.makeText(this, preference.getString("seq",""), Toast.LENGTH_SHORT).show();
        ok = (Button) findViewById(R.id.btn_ok);
        et_input = (EditText) findViewById(R.id.et_input);

        ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.getBackground().setAlpha(255);

                        if (et_input.getText().toString().equals("")) {
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Empty Text Field !!!");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();


                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);

                            et_input.setText("");

                        } else {

                            if (preference.getString("pin", "").equals(et_input.getText().toString())) {

                                finish();

                            } else {
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.toast,
                                        (ViewGroup) findViewById(R.id.toast_layout_root));

                                ImageView image = (ImageView) layout.findViewById(R.id.image);
                                image.setImageResource(R.drawable.logo);
                                TextView text = (TextView) layout.findViewById(R.id.text);
                                text.setText("Invalid PIN !!!");

                                Toast toast = new Toast(getApplicationContext());
                                toast.setGravity(Gravity.BOTTOM, 0, 110);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(layout);
                                toast.show();


                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(500);

                                et_input.setText("");
                            }

                        }


                        break;
                }
                return false;
            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();

        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            finish();
            return false;

        }
        return super.onKeyDown(keyCode, event);
    }

}