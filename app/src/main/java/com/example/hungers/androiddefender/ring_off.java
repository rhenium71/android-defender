package com.example.hungers.androiddefender;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ring_off extends Service {
    private WindowManager wm;
    private RelativeLayout ll;
    private TextView txt;

    private android.widget.Button ok;
    public static String PREFS_NAME="hungers";

    final Camera camera=Camera.open();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        Camera.Parameters p = camera.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(p);
        camera.startPreview();

        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        ll = new RelativeLayout(this);
        txt = new TextView(this);
        ok = new Button(this);






        RelativeLayout.LayoutParams llParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        ll.setBackgroundResource(R.drawable.circle); //or whatever your image is
        ll.setGravity(Gravity.CENTER);
        ll.setLayoutParams(llParameters);

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(300,300,WindowManager.LayoutParams.TYPE_PHONE,WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        parameters.x = 0;
        parameters.y = 0;
        parameters.gravity = Gravity.CENTER_HORIZONTAL;

        LinearLayout ll1 = new LinearLayout(this);
        ll1.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(0, 0, 0, 0);

        ViewGroup.LayoutParams text= new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        ViewGroup.LayoutParams btn = new ViewGroup.LayoutParams(150,150);

      /*  txt.setText("Are you want to stop?");
        txt.setLayoutParams(text);
        txt.setTextSize(25);
        txt.setTextColor(Color.argb(255, 61, 172, 225));
        txt.setGravity(Gravity.CENTER);
*/




        ok.setBackgroundResource(R.drawable.on_off);
        //ok.setText("OFF");
        ok.setLayoutParams(btn);
        //ok.setTextSize(15);
        //ok.setBackgroundColor(Color.argb(255, 0, 0, 0));
        ok.setTextColor(Color.argb(255, 61, 172, 225));




        wm.addView(ll,parameters);
        //ll1.addView(ok,layoutParams);

        ll.addView(ok);




        ll.setOnTouchListener(new View.OnTouchListener() {
            private WindowManager.LayoutParams update = parameters;
            int x, y;
            float touchX, touchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        x = update.x;
                        y = update.y;

                        touchX = event.getRawX();
                        touchY = event.getRawY();

                        break;

                    case MotionEvent.ACTION_MOVE:

                        update.x = (int) (x + (event.getRawX() - touchX));
                        update.y = (int) (y + (event.getRawY() - touchY));

                        wm.updateViewLayout(ll, update);


                        break;

                    default:

                        break;

                }


                return false;
            }
        });




        ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.getBackground().setAlpha(180);
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.getBackground().setAlpha(255);

                        editor.putString("sound","off");
                        editor.commit();

                        Receiver.MP.stop();
                        Camera.Parameters p = camera.getParameters();
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(p);
                        camera.stopPreview();
                        camera.release();
                        wm.removeView(ll);
                        stopSelf();

                        break;
                }
                return false;
            }
        });


    };
}
