package com.example.hungers.androiddefender;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

public class temp extends AppCompatActivity {

    public final static int REQUEST_CODE = 10101;
    public static String PREFS_NAME="hungers";
    public static SwitchCompat s1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        s1 = (SwitchCompat) findViewById(R.id.s1);
        TextView tv = (TextView) findViewById(R.id.tv_on);
        TextView text = (TextView) findViewById(R.id.tv);
        TextView text1 = (TextView) findViewById(R.id.tv1);
        TextView text2 = (TextView) findViewById(R.id.tv2);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv.setTypeface(type1);
        text1.setTypeface(type1);
        text2.setTypeface(type1);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        text.setTypeface(type);


        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                s1.setChecked(false);
            } else {
                if(preference.getString("short_cut","").equals("off") || preference.getString("short_cut","") == null){
                    s1.setChecked(false);
                }
                else if(preference.getString("short_cut","").equals("on") )
                {
                    s1.setChecked(true);
                }
            }
        }
        else{
            if(preference.getString("short_cut","").equals("off") || preference.getString("short_cut","") == null){
                s1.setChecked(false);
            }
            else if(preference.getString("short_cut","").equals("on") )
            {
                s1.setChecked(true);
            }
        }
        s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        if (!Settings.canDrawOverlays(getApplicationContext())) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, REQUEST_CODE);
                        }
                        else{

                            /*try{
                                if(preference.getString("x",null).equals("0") && preference.getString("y",null).equals("0")){
                                    torch_floatingWindow.parameters.x = 0;
                                    torch_floatingWindow.parameters.y = 0;
                                }
                                torch_floatingWindow.wm.addView(torch_floatingWindow.ll,torch_floatingWindow.parameters);
                            }
                            catch(NullPointerException e){*/
                                startService(new Intent(getApplicationContext(),torch_floatingWindow.class));
                            //}
                        }
                    }
                    else{


                       /* try{
                            if(preference.getString("x",null).equals("0") && preference.getString("y",null).equals("0")){
                                torch_floatingWindow.parameters.x = 0;
                                torch_floatingWindow.parameters.y = 0;
                            }
                            torch_floatingWindow.wm.addView(torch_floatingWindow.ll,torch_floatingWindow.parameters);
                            Toast.makeText(temp.this, "add view", Toast.LENGTH_SHORT).show();
                        }
                        catch(NullPointerException e){*/
                            startService(new Intent(temp.this,torch_floatingWindow.class));
                            //Toast.makeText(temp.this, "Start", Toast.LENGTH_SHORT).show();
                        //}

                        editor.putString("short_cut","on");
                        editor.commit();

                    }


                }
                else {
                    try{
                        torch_floatingWindow.wm.removeView(torch_floatingWindow.ll);
                        stopService(new Intent(temp.this,torch_floatingWindow.class));
                    }
                    catch(NullPointerException e){
                        startService(new Intent(getApplicationContext(),torch_floatingWindow.class));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                torch_floatingWindow.wm.removeView(torch_floatingWindow.ll);

                            }
                        }, 1000);

                    }

                    editor.putString("short_cut","off");
                    editor.commit();
                }
            }
        });

        /*tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(preference.getString("short_cut","").equals("off") || preference.getString("short_cut","") == null){
                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        if (!Settings.canDrawOverlays(getApplicationContext())) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, REQUEST_CODE);

                        }
                        else{
                            startService(new Intent(temp.this,torch_floatingWindow.class));
                            editor.putString("short_cut","on");
                            editor.commit();
                            s1.setChecked(true);
                        }
                    }
                    else{
                        startService(new Intent(temp.this,torch_floatingWindow.class));
                        s1.setChecked(true);
                        editor.putString("short_cut","on");
                        editor.commit();
                    }
                }
            }
        });*/

    }
    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (requestCode == REQUEST_CODE) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(this)) {
                    startService(new Intent(temp.this, torch_floatingWindow.class));
                    editor.putString("short_cut", "on");
                    editor.commit();
                    s1.setChecked(true);
                }
            }
        }
    }


}
