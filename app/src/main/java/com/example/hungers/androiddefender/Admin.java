package com.example.hungers.androiddefender;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class Admin extends DeviceAdminReceiver {
    public DevicePolicyManager DPM;
    public ComponentName NM;
    public static String PREFS_NAME="hungers";


    @Override
    public void onEnabled(final Context context, Intent intent) {
        Toast.makeText(context," Defender's Adminstrative Power is Enabled!!",Toast.LENGTH_LONG).show();

        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.startService(new Intent(context.getApplicationContext(),recent_activity.class));
            }
        }, 5000);


    }

    public void onDisabled(Context context, Intent intent) {
        Toast.makeText(context,"Defender's Adminstrative Power is Disabled!!",Toast.LENGTH_LONG).show();
    }
}