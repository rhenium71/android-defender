package com.example.hungers.androiddefender.power_button;

import android.app.KeyguardManager;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.hungers.androiddefender.Admin;
import com.example.hungers.androiddefender.R;

public class PowerButtonService extends Service {
    public DevicePolicyManager DPM;
    public ComponentName NM;

    public PowerButtonService() {
    }
    @Override
    public void onCreate() {
        super.onCreate();
        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {

            //home or recent button
            public void onCloseSystemDialogs(String reason) {
                if ("globalactions".equals(reason)) {
                    KeyguardManager myKM = (KeyguardManager) getApplication().getSystemService(Context.KEYGUARD_SERVICE);
                    if( myKM.inKeyguardRestrictedInputMode()) {
                        //it is locked
                        DPM = (DevicePolicyManager) getApplication().getSystemService(Context.DEVICE_POLICY_SERVICE);
                        NM = new ComponentName(getApplication(), Admin.class);
                        DPM.setPasswordQuality(NM, DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                        DPM.lockNow();
                    } else {
                        //it is not locked
                    }

                    //Log.i("Key", "Long press on power button");
                } /*else if ("homekey".equals(reason)) {
                    //home key pressed
                    //Toast.makeText(PowerButtonService.this, "home", Toast.LENGTH_SHORT).show();
                    *//*Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(1000);*//*
                } else if ("recentapps".equals(reason)) {
                    // recent apps button clicked
                }*/
            }

            /*@Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_POWER) {

                    Log.i("Key", "keycode " + event.getKeyCode());
                    //Toast.makeText(PowerButtonService.this, "power", Toast.LENGTH_SHORT).show();
                    *//*Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(1000);*//*

                }
                return super.dispatchKeyEvent(event);
            }*/
        };

        mLinear.setFocusable(true);

        View mView = LayoutInflater.from(this).inflate(R.layout.service_layout, mLinear);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        //params
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                1,
                1,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_FULLSCREEN
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;
        wm.addView(mView, params);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
