package com.example.hungers.androiddefender;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.test.mock.MockPackageManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hungers.androiddefender.power_button.PowerButtonService;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;


import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;
import pl.droidsonroids.gif.GifTextView;

import static com.example.hungers.androiddefender.temp.REQUEST_CODE;

public class MainActivity extends AppCompatActivity {

    public static String PREFS_NAME="hungers";
    public DevicePolicyManager DPM;
    public ComponentName NM;

    ImageView iv1,iv3,iv4;
    GifTextView iv2,iv5;
    private static final int REQUEST_CODE_PERMISSION_Ca = 0;
    private static final int REQUEST_CODE_PERMISSION_Co = 1;
    private static final int REQUEST_CODE_PERMISSION_L = 2;
    private static final int REQUEST_CODE_PERMISSION_P = 3;
    private static final int REQUEST_CODE_PERMISSION_SMS = 4;
    private static final int REQUEST_CODE_PERMISSION_S = 5;

    public final static int REQUEST_CODE = 10101;

    private BoomMenuButton bmb0,bmb1,bmb2,bmb3,bmb4;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_main);
        initUI();
        //startService(new Intent(this, PowerButtonService.class));
        //for add

       /* MobileAds.initialize(getApplicationContext(), "ca-app-pub-2337337799695171~7163194840");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);*/

        ////////////////


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        editor.putString("home","false");
        editor.commit();


        DPM=(DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        NM=new ComponentName(this,Admin.class);

       /* DPM.setPasswordMinimumLength(NM, 0);
        DPM.resetPassword("", DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);
*/
        //for exit


        DisplayMetrics dm = getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);

        double scd = dm.widthPixels / screenInches;

        //Toast.makeText(this, String.valueOf(dm.heightPixels), Toast.LENGTH_SHORT).show();

        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("EXIT", false)) {


            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast,
                    (ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.logo);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText("Thanks for staying with HUNGERS");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 110);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();

            finish();
        }

        /*if (Build.VERSION.SDK_INT >= 23) {

            Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            startActivity(myIntent);

            // Call some material design APIs here
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    3);
        }
        else {
            // Implement this feature without material design

        }*/

        if (Build.VERSION.SDK_INT >= 23) {
            String mPermission_SMS = Manifest.permission.SEND_SMS;
            String mPermission_p = Manifest.permission.CALL_PHONE;
            String mP = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            String mPermission_location = Manifest.permission.ACCESS_FINE_LOCATION;
            boolean s=false;
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CAMERA,
                                                                     Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                                     Manifest.permission.CALL_PHONE,
                                                                     Manifest.permission.SEND_SMS},REQUEST_CODE_PERMISSION_P);
                if (!Settings.canDrawOverlays(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, REQUEST_CODE);
                }
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION_P) {
            if (grantResults.length == 1 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED ) {

               // Toast.makeText(this, "Location", Toast.LENGTH_SHORT).show();
                if (!Settings.canDrawOverlays(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, REQUEST_CODE);
                }
            }
            else{
                // Failure Stuff
            }
        }


    }
    /*@Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(MainActivity.this, "Permission to read your External storage", Toast.LENGTH_SHORT).show();
                } else {


                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }

                return;
            }



        }
    }*/
    private void initUI() {

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final ViewPager viewPager = (ViewPager) findViewById(R.id.vp_horizontal_ntb);
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 5;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {
                final View view0 = LayoutInflater.from(
                        getBaseContext()).inflate(R.layout.track, null, false);
                final View view1 = LayoutInflater.from(
                        getBaseContext()).inflate(R.layout.security, null, false);
                final View view2 = LayoutInflater.from(
                        getBaseContext()).inflate(R.layout.others, null, false);
                final View view3 = LayoutInflater.from(
                        getBaseContext()).inflate(R.layout.about, null, false);
                final View view4 = LayoutInflater.from(
                        getBaseContext()).inflate(R.layout.settings, null, false);
                

                if (position == 0) {

                    TextView text = (TextView)view0.findViewById(R.id.tv_loc);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
                    text.setTypeface(type);

                    iv1=(ImageView) view0.findViewById(R.id.iv_track);
                    bmb0 = (BoomMenuButton) view0.findViewById(R.id.bmb);
                    assert bmb0 != null;
                    bmb0.setButtonEnum(ButtonEnum.Ham);
                    bmb0.setPiecePlaceEnum(PiecePlaceEnum.HAM_4);
                    bmb0.setButtonPlaceEnum(ButtonPlaceEnum.HAM_4);
                    for (int i = 0; i < bmb0.getPiecePlaceEnum().pieceNumber(); i++){
                        bmb0.addBuilder(BuilderManager.getHamButtonBuilder0());

                    }

                    bmb0.setOnBoomListener(new OnBoomListenerAdapter() {
                        @Override
                        public void onBoomWillShow() {
                            super.onBoomWillShow();
                            // logic here
                        }
                    });


                    bmb0.setOnBoomListener(new OnBoomListener() {
                        @Override
                        public void onClicked(int index, BoomButton boomButton) {

                            if(index == 0){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);

                                        editor.putString("seq","1");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 1){

                                   //
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,temp.class);
                                        startActivity(i);

                                        editor.putString("seq","1");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 2){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(MainActivity.this,MapsActivity2.class);
                                        startActivity(i);
                                    }
                                }, 950);

                            }
                            else if(index == 3){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,location3.class);
                                        startActivity(i);
                                    }
                                }, 950);

                            }

                        }

                        @Override
                        public void onBackgroundClick() {

                        }

                        @Override
                        public void onBoomWillHide() {

                        }

                        @Override
                        public void onBoomDidHide() {
                            iv1.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onBoomWillShow() {

                            iv1.setVisibility(View.INVISIBLE);

                        }

                        @Override
                        public void onBoomDidShow() {

                        }
                    });


                    container.addView(view0);
                    return view0;


                } else if (position == 1) {
                    TextView text = (TextView)view1.findViewById(R.id.tv_sec);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
                    text.setTypeface(type);
                    iv2=(GifTextView)view1.findViewById(R.id.iv_2);
                    bmb1 = (BoomMenuButton) view1.findViewById(R.id.bmb);
                    assert bmb1 != null;
                    bmb1.setButtonEnum(ButtonEnum.TextInsideCircle);
                    bmb1.setPiecePlaceEnum(PiecePlaceEnum.DOT_6_4);
                    bmb1.setButtonPlaceEnum(ButtonPlaceEnum.SC_6_6);
                    for (int i = 0; i < bmb1.getPiecePlaceEnum().pieceNumber(); i++){
                        bmb1.addBuilder(BuilderManager.getTextInsideCircleButtonBuilder());

                    }
                    bmb1.setOnBoomListener(new OnBoomListenerAdapter() {
                        @Override
                        public void onBoomWillShow() {
                            super.onBoomWillShow();
                            // logic here
                        }
                    });


                    bmb1.setOnBoomListener(new OnBoomListener() {
                        @Override
                        public void onClicked(int index, BoomButton boomButton) {
                            if(index == 0){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                        editor.putString("seq","2");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 1){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                        editor.putString("seq","3");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 2){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                        editor.putString("seq","4");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 3){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                        editor.putString("seq","5");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 4){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                        editor.putString("seq","6");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                            else if(index == 5){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                        editor.putString("seq","7");
                                        editor.commit();
                                    }
                                }, 950);

                            }
                        }

                        @Override
                        public void onBackgroundClick() {

                        }

                        @Override
                        public void onBoomWillHide() {

                        }

                        @Override
                        public void onBoomDidHide() {
                            iv2.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onBoomWillShow() {
                            iv2.setVisibility(View.INVISIBLE);

                        }

                        @Override
                        public void onBoomDidShow() {

                        }
                    });
                    container.addView(view1);
                    return view1;
                } else if (position == 2) {
                    TextView text = (TextView)view2.findViewById(R.id.tv_others);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
                    text.setTypeface(type);
                    iv3=(ImageView) view2.findViewById(R.id.iv_3);
                    bmb2 = (BoomMenuButton) view2.findViewById(R.id.bmb);
                    assert bmb2 != null;
                    bmb2.setButtonEnum(ButtonEnum.Ham);
                    bmb2.setPiecePlaceEnum(PiecePlaceEnum.HAM_4);
                    bmb2.setButtonPlaceEnum(ButtonPlaceEnum.HAM_4);
                    for (int i = 0; i < bmb2.getPiecePlaceEnum().pieceNumber(); i++){
                        bmb2.addBuilder(BuilderManager.getHamButtonBuilder1());

                    }

                    bmb2.setOnBoomListener(new OnBoomListenerAdapter() {
                        @Override
                        public void onBoomWillShow() {
                            super.onBoomWillShow();
                            // logic here
                        }
                    });


                    bmb2.setOnBoomListener(new OnBoomListener() {
                        @Override
                        public void onClicked(int index, BoomButton boomButton) {

                            if(index == 0){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        editor.putString("switch_off","true");
                                        editor.commit();
                                        Intent i=new Intent(MainActivity.this,LockScreen.class);
                                        startActivity(i);
                                    }
                                }, 950);

                            }
                            else if(index == 1){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        /*if(preference.getString("x",null).equals("0") && preference.getString("y",null).equals("0")){
                                            torch_floatingWindow.parameters.x = 0;
                                            torch_floatingWindow.parameters.y = 0;
                                        }
                                        torch_floatingWindow.wm.addView(torch_floatingWindow.ll,torch_floatingWindow.parameters);
*/

                                        Intent i=new Intent(MainActivity.this,temp.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 2){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        LayoutInflater inflater = getLayoutInflater();
                                        View layout = inflater.inflate(R.layout.toast,
                                                (ViewGroup) findViewById(R.id.toast_layout_root));

                                        ImageView image = (ImageView) layout.findViewById(R.id.image);
                                        image.setImageResource(R.drawable.logo);
                                        TextView text = (TextView) layout.findViewById(R.id.text);
                                        text.setText("This feature is under developing");

                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setGravity(Gravity.BOTTOM, 0, 110);
                                        toast.setDuration(Toast.LENGTH_LONG);
                                        toast.setView(layout);
                                        toast.show();

                                    }
                                }, 950);

                            }
                            else if(index == 3){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        LayoutInflater inflater = getLayoutInflater();
                                        View layout = inflater.inflate(R.layout.toast,
                                                (ViewGroup) findViewById(R.id.toast_layout_root));

                                        ImageView image = (ImageView) layout.findViewById(R.id.image);
                                        image.setImageResource(R.drawable.logo);
                                        TextView text = (TextView) layout.findViewById(R.id.text);
                                        text.setText("This feature is under developing");

                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setGravity(Gravity.BOTTOM, 0, 110);
                                        toast.setDuration(Toast.LENGTH_LONG);
                                        toast.setView(layout);
                                        toast.show();
                                    }
                                }, 950);

                            }



                        }

                        @Override
                        public void onBackgroundClick() {

                        }

                        @Override
                        public void onBoomWillHide() {

                        }

                        @Override
                        public void onBoomDidHide() {
                            iv3.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onBoomWillShow() {
                            iv3.setVisibility(View.INVISIBLE);

                        }

                        @Override
                        public void onBoomDidShow() {

                        }
                    });


                    container.addView(view2);
                    return view2;
                } else if (position == 3) {
                    TextView text = (TextView)view3.findViewById(R.id.tv_ab);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
                    text.setTypeface(type);
                    iv4=(ImageView) view3.findViewById(R.id.iv_4);
                    bmb3 = (BoomMenuButton) view3.findViewById(R.id.bmb);
                    assert bmb3 != null;
                    bmb3.setButtonEnum(ButtonEnum.TextInsideCircle);
                    bmb3.setPiecePlaceEnum(PiecePlaceEnum.DOT_4_2);
                    bmb3.setButtonPlaceEnum(ButtonPlaceEnum.SC_4_2);
                    for (int i = 0; i < bmb3.getPiecePlaceEnum().pieceNumber(); i++){
                        bmb3.addBuilder(BuilderManager.getTextInsideCircleButtonBuilderWithDifferentPieceColor());

                    }
                    bmb3.setOnBoomListener(new OnBoomListenerAdapter() {
                        @Override
                        public void onBoomWillShow() {
                            super.onBoomWillShow();
                            // logic here
                        }
                    });


                    bmb3.setOnBoomListener(new OnBoomListener() {
                        @Override
                        public void onClicked(int index, BoomButton boomButton) {
                            if(index == 0){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,about_us.class);
                                        startActivity(i);
                                    }
                                }, 950);

                            }
                            else if(index == 1){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,help.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 2){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,rate.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 3){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i=new Intent(MainActivity.this,about_app.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                        }

                        @Override
                        public void onBackgroundClick() {

                        }

                        @Override
                        public void onBoomWillHide() {

                        }

                        @Override
                        public void onBoomDidHide() {
                            iv4.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onBoomWillShow() {
                            iv4.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onBoomDidShow() {

                        }
                    });
                    container.addView(view3);
                    return view3;
                } else{
                    TextView text = (TextView)view4.findViewById(R.id.tv_set);
                    Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
                    text.setTypeface(type);
                    iv5=(GifTextView) view4.findViewById(R.id.iv_5);
                    bmb4 = (BoomMenuButton) view4.findViewById(R.id.bmb);
                    assert bmb4 != null;
                    bmb4.setButtonEnum(ButtonEnum.Ham);
                    bmb4.setPiecePlaceEnum(PiecePlaceEnum.HAM_5);
                    bmb4.setButtonPlaceEnum(ButtonPlaceEnum.HAM_5);
                    for (int i = 0; i < bmb4.getPiecePlaceEnum().pieceNumber(); i++){
                        bmb4.addBuilder(BuilderManager.getHamButtonBuilder());

                    }


                    // Use OnBoomListenerAdapter to listen part of methods
                    bmb4.setOnBoomListener(new OnBoomListenerAdapter() {
                        @Override
                        public void onBoomWillShow() {
                            super.onBoomWillShow();
                            // logic here
                        }
                    });

                    // Use OnBoomListener to listen all methods
                    bmb4.setOnBoomListener(new OnBoomListener() {
                        @Override
                        public void onClicked(int index, BoomButton boomButton) {
                            // If you have implement listeners for boom-buttons in builders,
                            // then you shouldn't add any listener here for duplicate callbacks.
                            if(index == 0){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,change_pin.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 1){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,change_safe_num.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 2){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i=new Intent(MainActivity.this,change_key.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 3){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i=new Intent(MainActivity.this,Settings_lockScr.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }
                            else if(index == 4){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i=new Intent(MainActivity.this,forgot.class);
                                        startActivity(i);

                                    }
                                }, 950);

                            }

                        }

                        @Override
                        public void onBackgroundClick() {

                        }

                        @Override
                        public void onBoomWillHide() {

                        }

                        @Override
                        public void onBoomDidHide() {
                            iv5.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onBoomWillShow() {
                            iv5.setVisibility(View.INVISIBLE);

                        }

                        @Override
                        public void onBoomDidShow() {

                        }
                    });

                    container.addView(view4);
                    return view4;
                }

            }
        });

        final String[] colors = getResources().getStringArray(R.array.vertical_ntb);
        final int bgColor = Color.parseColor("#1A1C23");
        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.track1),
                        Color.parseColor(colors[0]))
                        .title("Track")
                        .badgeTitle("Setting1")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.security),
                        Color.parseColor(colors[1]))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Security")
                        .badgeTitle("Setting1")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.other1),
                        Color.parseColor(colors[2]))
                        .title("Others")
                        .badgeTitle("state")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.about),
                        Color.parseColor(colors[3]))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("About")
                        .badgeTitle("Setting1")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.set),
                        Color.parseColor(colors[4]))
                        .title("Settings")
                        .badgeTitle("777")
                        .build()
        );

        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 0);
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 5);
                }
            }
        }, 5);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        //startService(new Intent(MainActivity.this,torch_floatingWindow.class));

    }

    @Override
    protected void onResume() {
        super.onResume();


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();




        DevicePolicyManager mDPM=(DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName mAdmin=new ComponentName(this,Admin.class);

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","off");
            editor.commit();
        }

        if(!mDPM.isAdminActive(mAdmin))
        {
            Intent intent=new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,mAdmin);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "To Take Security Measures");
            startActivity(intent);
        }

        if(preference.getString("service","").equals("off")){
            startService(new Intent(getApplicationContext(),recent_activity.class));
            editor.putString("count","1");
            editor.commit();
        }

        if(preference.getString("key",null)==null) {

            Intent i = new Intent(MainActivity.this, set_key_word.class);
            startActivity(i);
        }

        if(preference.getString("safe_number",null)==null) {

            Intent i = new Intent(MainActivity.this, set_safe_number.class);
            startActivity(i);
        }

        if(preference.getString("pin",null)==null) {

            Intent i = new Intent(MainActivity.this, set_pin.class);
            startActivity(i);

            if (android.os.Build.VERSION.SDK_INT < 23) {
                //startService(new Intent(getApplicationContext(),torch_floatingWindow.class));
            }


            editor.putString("searchPhone", "on");
            editor.putString("lockPhone", "on");
            editor.putString("changePass", "on");
            editor.putString("call", "on");
            editor.putString("wipe", "on");
            editor.putString("cam", "on");
            editor.putString("rebootSMS", "on");
            editor.putString("rebootLock", "on");
            editor.putString("lock_scren","on");
            editor.putString("service","off");
            editor.putString("tracked_lat","0");
            editor.putString("tracked_lang","0");
            editor.putString("count","1");
            editor.putString("f","on");
            editor.putString("l","on");
            editor.putString("x","0");
            editor.putString("ring","2");
            editor.putString("sound","off");
            editor.putString("y","0");
            editor.putString("send_sms","false");
            editor.putString("EC","off");
            editor.putString("cam","off");
            editor.putString("short_cut","off");
            editor.putString("call_security","on");
            editor.putString("latitude","0");
            editor.putString("longitude","0");
            editor.putString("loc_on","false");
            editor.putString("req_for_loc_name","false");



            editor.commit();

        }

        //final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        /*try{
            if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER)
                    && preference.getString("loc_on","").equals("true")) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent i = new Intent(MainActivity.this, location.class);
                        startActivity(i);

                    }
                }, 1000);

                editor.putString("loc_on","false");
                editor.commit();
            }
        }
        catch (NullPointerException e){

        }*/
    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Intent intent = new Intent(getApplicationContext(),reboot_alert.class);
            startActivity(intent);

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
