package com.example.hungers.androiddefender;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class rate extends Activity {
    public static String PREFS_NAME="hungers";

    TextView tv;
    FrameLayout ll;
    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams windowManager=getWindow().getAttributes();
        windowManager.dimAmount=0.65f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_rate);

        tv=(TextView) findViewById(R.id.tv1);
        b1=(Button) findViewById(R.id.btn_later);
        b2=(Button) findViewById(R.id.btn_star);

        ll =(FrameLayout) findViewById(R.id.fl1);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        tv.setTypeface(type);
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        b1.setTypeface(type1);
        b2.setTypeface(type1);

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b1.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b1.getBackground().setAlpha(255);



                        break;
                }
                return false;
            }
        });

        b2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b2.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b2.getBackground().setAlpha(255);



                        break;
                }
                return false;
            }
        });



    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }
}
