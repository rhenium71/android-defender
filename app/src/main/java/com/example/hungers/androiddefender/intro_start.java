package com.example.hungers.androiddefender;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.WindowDecorActionBar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xenione.digit.TabDigit;

import java.util.Timer;
import java.util.TimerTask;

public class intro_start extends AppCompatActivity implements Runnable{

    TabDigit tabDigit1;
    public static String PREFS_NAME="hungers";
    private static final char[] HOURS = new char[]{'3', '2', '1','0'};

    private long elapsedTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro_start);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        //for exit


        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("EXIT", false)) {


            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast,
                    (ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.logo);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText("Exit Android Defender");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 110);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();


            finish();
        }



        //end//for exit


        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("EXIT", false)) {


            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast,
                    (ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.logo);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText("Exit Android Defender");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 110);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();


            finish();
        }



        //end

        tabDigit1=(TabDigit) findViewById(R.id.tabDigit1);



        tabDigit1.setChars(HOURS);
        tabDigit1.setTextSize(100);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tabDigit1.start();
                ViewCompat.postOnAnimationDelayed(tabDigit1, this, 1000);

            }
        }, 500);
        /*final Handler handler= new Handler();
        final Timer timer = new Timer();
        //Toast.makeText(getApplicationContext(),"start" , Toast.LENGTH_SHORT).show();
        final TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {

                    }
                });
            }
        };
        timer.schedule(timerTask, 0,3000);
        */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
                Intent i=new Intent(intro_start.this,MainActivity.class);
                startActivity(i);
            }
        }, 3500);

    }
    @Override
    public void run() {

        tabDigit1.start();
        ViewCompat.postOnAnimationDelayed(tabDigit1, this, 1000);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStop() {
        super.onStop();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

    }

}
