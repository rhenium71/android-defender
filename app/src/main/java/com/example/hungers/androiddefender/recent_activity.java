package com.example.hungers.androiddefender;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;

public class recent_activity extends Service {
    public static String PREFS_NAME = "hungers";
    BroadcastReceiver receiver;

    int checker = 0, locker = 0;
    KeyEvent event;

    @Override
    public IBinder onBind(Intent intent) {

        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.widget);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.notific).setContent(
                remoteViews);

        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.button1, resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        startForeground(998, mBuilder.build());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();


        if (preference.getString("service", "") == null) {
            editor.putString("service", "off");
            editor.commit();
        }
        final Handler handler = new Handler();
        final Timer timer = new Timer();

        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        try{
                            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
                            filter.addAction(Intent.ACTION_SCREEN_OFF);

                            receiver = new Screen_ON_OFF();
                            registerReceiver(receiver, filter);
                        }
                        catch (IllegalArgumentException e){

                        }


                    }
                });
            }
        };
        timer.schedule(timerTask, 0, 600000);

        final Handler handler1 = new Handler();
        final Timer timer1 = new Timer();
        final TimerTask timerTask2 = new TimerTask() {
            @Override
            public void run() {
                handler1.post(new Runnable() {

                    @Override
                    public void run() {
                       /* if(preference.getString("short_cut","").equals("on")){
                            if(preference.getString("service_floating","").equals("off")){

                            }
                        }*/

                        if (Build.VERSION.SDK_INT >= 22) {
                            String topPackageName = null;
                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                @SuppressWarnings("WrongConstant")
                                UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService("usagestats");
                                long time = System.currentTimeMillis();
                                // We get usage stats for the last 10 seconds
                                List<UsageStats> stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 100*10, time);
                                // Sort the stats by the last tim e used
                                if(stats != null) {
                                    SortedMap<Long,UsageStats> mySortedMap = new TreeMap<Long,UsageStats>();
                                    for (UsageStats usageStats : stats) {
                                        mySortedMap.put(usageStats.getLastTimeUsed(),usageStats);
                                    }
                                    if(!mySortedMap.isEmpty()) {
                                        topPackageName =  mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                                    }
                                }
                            }
                            try{

                                if (topPackageName.equals("com.android.settings")) {
                                    checker++;

                                } else {
                                    checker = 0;
                                }

                                if (checker == 1 ) {

                                    //startService(new Intent(getApplicationContext(),LockScr.class));

                                }

                            } catch(NullPointerException e){
                                checker = 0;
                            }
                            //Toast.makeText(recent_activity.this, topPackageName, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            final ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                            final List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                            final ComponentName componentName = taskInfo.get(0).topActivity;

                            DevicePolicyManager mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                            ComponentName mAdminName = new ComponentName(getApplicationContext(), Admin.class);


                            if (componentName.getClassName().equals("com.android.settings.DeviceAdminAdd")
                                    || componentName.getClassName().equals("com.example.hungers.androiddefender.DAdmin_lock")
                                    ) {
                                checker++;

                            } else {
                                checker = 0;
                                if(locker == 1){
                                    stopService(new Intent(recent_activity.this,DAdmin_lock.class));
                                    locker = 0;
                                }
                            }


                            if (checker == 1 && mDPM.isAdminActive(mAdminName)) {

                                /*Intent dialogIntent = new Intent(getApplicationContext(), admin_lock.class);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(dialogIntent);*/
                                startService(new Intent(recent_activity.this,DAdmin_lock.class));
                                locker = 1;
                            }

                        }
                        }
                });
            }
        };
        timer1.schedule(timerTask2, 0, 100);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        receiver = new Screen_ON_OFF();
        unregisterReceiver(receiver);
    }
}
