package com.example.hungers.androiddefender;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.widget.Toast;

public class SmsResultReceiver extends BroadcastReceiver {
    public SmsResultReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preference=context.getSharedPreferences(Receiver.PREFS_NAME,Context.MODE_PRIVATE);

        switch(getResultCode())
        {
            case Activity.RESULT_OK:

                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Intent act1=new Intent(context,SmsResultReceiver.class);
                PendingIntent sentPI1 = PendingIntent.getBroadcast(context, 0,act1, 0);
                Toast.makeText(context.getApplicationContext(), "send sms result error", Toast.LENGTH_SHORT).show();
                SmsManager sms1=SmsManager.getDefault();
                sms1.sendTextMessage(preference.getString("safe_number", null), null, "Sim Card Changed1", sentPI1, null);
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                Intent act2=new Intent(context,SmsResultReceiver.class);
                PendingIntent sentPI2= PendingIntent.getBroadcast(context, 0,act2, 0);
                Toast.makeText(context.getApplicationContext(), "send sms no service", Toast.LENGTH_SHORT).show();
                SmsManager sms2=SmsManager.getDefault();
                sms2.sendTextMessage(preference.getString("safe_number", null), null, "Sim Card Changed2", sentPI2, null);
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Intent act3=new Intent(context,SmsResultReceiver.class);
                PendingIntent sentPI3= PendingIntent.getBroadcast(context, 0,act3, 0);
                Toast.makeText(context.getApplicationContext(), "send sms radio off", Toast.LENGTH_SHORT).show();
                SmsManager sms3=SmsManager.getDefault();
                sms3.sendTextMessage(preference.getString("safe_number", null), null, "Sim Card Changed3", sentPI3, null);
                break;
        }
    }
}