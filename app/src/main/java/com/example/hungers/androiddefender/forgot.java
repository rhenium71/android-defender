package com.example.hungers.androiddefender;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class forgot extends AppCompatActivity {
    public static String PREFS_NAME="hungers";

    static final int PICK_CONTACT_REQUEST = 1;

    Button send,contact,a_send;
    EditText et_num,et_pin,et_pass;
    TextView tv_name;

    LinearLayout l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();


        send=(Button) findViewById(R.id.send1);
        a_send=(Button) findViewById(R.id.send_again1);
        contact=(Button) findViewById(R.id.contact1);

        et_num=(EditText) findViewById(R.id.et_number1);
        et_pin=(EditText) findViewById(R.id.et_pin1);
        et_pass=(EditText) findViewById(R.id.et_pass1);

        tv_name=(TextView) findViewById(R.id.tv_name);

        l=(LinearLayout) findViewById(R.id.llh);

        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),how_work.class);
                startActivity(i);
            }
        });

        contact.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        contact.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        contact.getBackground().setAlpha(255);

                        editor.putString("contact_flag","true");
                        editor.commit();

                        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
                        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);

                        break;
                }

                return false;
            }
        });


        send.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        send.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        send.getBackground().setAlpha(255);

                        if (et_num.getText().toString().equals("")|| et_pin.getText().toString().equals("")
                                ||et_pass.getText().toString().equals("")) {

                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Text Fields are empty!!!");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);

                        }
                        else  {
                            editor.putString("numInputed",et_num.getText().toString());
                            editor.putString("pinInputed",et_pin.getText().toString());
                            editor.putString("keyInputed",et_pass.getText().toString());
                            editor.putString("forgot","true");
                            editor.commit();

                            Intent i = new Intent(getApplicationContext(),sms_alert.class);
                            startActivity(i);

                        }



                        break;
                }

                return false;
            }
        });

        a_send.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        a_send.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        a_send.getBackground().setAlpha(255);

                        try{

                            String msg = preference.getString("pinInputed", null);
                            msg = msg.concat(" "+"NP"+" "+preference.getString("keyInputed",""));
                            sendSMS(preference.getString("numInputed", null),msg);

                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("SMS is sending");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);
                        } catch(NullPointerException e){
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Please input first !!!");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);
                        }






                }

                return false;
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor=preference.edit();

        et_num.setText(preference.getString("contact_number",""));
        tv_name.setText(preference.getString("contact_name",""));


        if(preference.getString("send_sms", null).equals("true")){
            send_sms();
            editor.putString("send_sms","false");
            editor.commit();
        }

    }
    private void send_sms() {
        SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor=preference.edit();

        String msg = preference.getString("pinInputed", null);
        msg = msg.concat(" "+"NP"+" "+preference.getString("keyInputed",""));
        sendSMS(preference.getString("numInputed", null),msg);

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        ImageView image = (ImageView) layout.findViewById(R.id.image);
        image.setImageResource(R.drawable.logo);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("SMS is sending");

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 110);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();

        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vib.vibrate(500);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("contact_flag","").equals("true")){
            editor.putString("contact_flag","false");
            editor.commit();
        }
        else if(preference.getString("contact_flag","").equals("false")){
            finish();
        }



    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();

        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
            return false;

        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the URI that points to the selected contact

                Uri contactUri = data.getData();
                String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER};

                Cursor people = getContentResolver().query(contactUri, projection, null, null, null);
                people.moveToFirst();

                int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                String name ;
                String number;
                name   = people.getString(indexName);
                number = people.getString(indexNumber);

                SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor=preference.edit();

                editor.putString("contact_number",number);
                editor.putString("contact_name",name);
                editor.commit();


                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText(preference.getString("contact_number",null).toString());

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        }
    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }

    // for sms delivery report
    BroadcastReceiver sendBroadcastReceiver = new forgot.SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new forgot.DeliverReceiver();

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);


    }

    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "sms not delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast,
                    (ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.logo);
            TextView text = (TextView) layout.findViewById(R.id.text);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 110);
            toast.setView(layout);

            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    text.setText("SMS sent successfully");
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                    text.setText("Generic failure.Please send again");
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    text.setText("No service.Please send again");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    text.setText("No service.Please send again");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    text.setText("Radio off.Please send again");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();

                    break;
            }

        }
    }
}
