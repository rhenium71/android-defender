package com.example.hungers.androiddefender;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

public class torch_floatingWindow extends Service {
    public static WindowManager wm;
    public static RelativeLayout ll;
    public static String PREFS_NAME="hungers";
    public static android.widget.Button ok;
    public static WindowManager.LayoutParams parameters;
    public static int dispalyX,displayY;

    public static int i=0;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
       /* RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.notification);
        android.support.v4.app.NotificationCompat.Builder mBuilder = new android.support.v4.app.NotificationCompat.Builder(
                this).setContent(
                remoteViews);

        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.button1, resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        startForeground(999, mBuilder.build());*/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        //Toast.makeText(this, "Service", Toast.LENGTH_SHORT).show();
        editor.putString("service_floating","on");
        editor.commit();
        add();
        startForeground();
        return START_STICKY;
    }
    private void startForeground() {
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))// set ticker. you can set a string also .setTicker("your string")
                .setContentText("Floating View Running")
                .setSmallIcon(R.drawable.notific)//set your own logo
                .setContentIntent(null)
                .setOngoing(true)
                .build();
        startForeground(990,notification);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        //Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show();
        editor.putString("service_floating","off");
        editor.commit();
    }


    public void add(){


        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        ll = new RelativeLayout(this);
        ok = new Button(this);

        Display display=wm.getDefaultDisplay();
        Point dimension= new Point();
        display.getSize(dimension);

        dispalyX=dimension.x;
        displayY=dimension.y;

        DisplayMetrics dm = getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);

        double scd=displayY / screenInches;

        int dp_ll;
        int dp_ok;
        if(dm.heightPixels >1280){
            dp_ll = 190;
            dp_ok = 185;
        }
        else if(dm.heightPixels>=1000 && dm.heightPixels<=1280){
            dp_ll = 100;
            dp_ok = 95;
        }
        else{
            dp_ll = 80;
            dp_ok = 75;
        }
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        RelativeLayout.LayoutParams llParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        //or whatever your image is
        ll.setGravity(Gravity.CENTER);
        ll.setLayoutParams(llParameters);

        parameters = new WindowManager.LayoutParams(dp_ll,dp_ll,WindowManager.LayoutParams.TYPE_PRIORITY_PHONE,WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        if(preference.getString("x",null).equals("0") && preference.getString("y",null).equals("0")){
            parameters.x = 0;
            parameters.y = 0;
        }
        else{
            int xx=Integer.valueOf(preference.getString("x",""));
            int yy=Integer.valueOf(preference.getString("y",""));

            if(xx == 0 ){
                parameters.x = 0;
                parameters.y = 0;
            }
            else{
                parameters.x=xx;
                parameters.y=yy;
            }


        }

        parameters.gravity = Gravity.CENTER_HORIZONTAL;

        ViewGroup.LayoutParams btn = new ViewGroup.LayoutParams(dp_ok,dp_ok);


        ok.setBackgroundResource(R.drawable.android);
        ok.setLayoutParams(btn);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ok.getBackground().setAlpha(150);

            }
        }, 1100);


        wm.addView(ll,parameters);
        ll.addView(ok);


        ok.setOnTouchListener(new View.OnTouchListener() {
            private WindowManager.LayoutParams update = parameters;
            int x, y;
            float touchX, touchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        ok.getBackground().setAlpha(255);
                        //ll.getBackground().setAlpha(255);

                        x = update.x;
                        y = update.y;
                        touchX = event.getRawX();
                        touchY = event.getRawY();

                        break;

                    case MotionEvent.ACTION_MOVE:
                        startService(new Intent(getApplicationContext(),Catch.class));

                        update.x = (int) (x + (event.getRawX() - touchX));
                        update.y = (int) (y + (event.getRawY() - touchY));

                        ok.getBackground().setAlpha(255);

                        if(update.y >= ((displayY/2)-350)){
                            update.x=0;
                            update.y=(displayY/2)-200;
                            wm.updateViewLayout(ll, update);
                            if(i == 0){
                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(100);
                                i++;
                            }

                        }
                        else{
                            wm.updateViewLayout(ll, update);
                            i=0;
                        }

                        break;

                    case MotionEvent.ACTION_UP:
                        stopService(new Intent(getApplicationContext(),Catch.class));

                        String xx=String.valueOf(update.x);
                        String yy=String.valueOf(update.y);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ok.getBackground().setAlpha(150);

                            }
                        }, 1100);

                        //ll.getBackground().setAlpha(80);
                        //SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

                        editor.putString("x",xx);
                        editor.putString("y",yy);
                        editor.commit();
                        if(update.x == 0 && update.y == ((displayY/2)-200)){
                            remove();
                            stopSelf();
                            try{
                                temp.s1.setChecked(false);
                            }
                            catch(NullPointerException e){

                            }

                            editor.putString("x","0");
                            editor.putString("y","0");
                            editor.putString("short_cut","off");
                            editor.commit();
                        }

                        break;
                    default:

                        break;
                }
                return false;
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialogIntent = new Intent(getApplicationContext(), short_cut.class);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);
            }
        });
    }

    public void remove(){
        wm.removeView(ll);
    }


}
