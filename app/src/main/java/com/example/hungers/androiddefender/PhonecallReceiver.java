package com.example.hungers.androiddefender;

import java.util.Date;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import static android.content.Context.MODE_PRIVATE;

public class PhonecallReceiver extends BroadcastReceiver {

    //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

    public DevicePolicyManager DPM;
    public ComponentName NM;
    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private static String savedNumber;  //because the passed incoming is only valid in ringing


    public static String PREFS_NAME="hungers";




    @Override
    public void onReceive(Context context, Intent intent) {

        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        }
        else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }


            onCallStateChanged(context, state, number);
        }
    }

    //Derived classes should override these to respond to specific events of interest
    /*protected abstract void onIncomingCallReceived(Context ctx, String number, Date start);
    protected abstract void onIncomingCallAnswered(Context ctx, String number, Date start);
    protected abstract void onIncomingCallEnded(Context ctx, String number, Date start, Date end);

    protected abstract void onOutgoingCallStarted(Context ctx, String number, Date start);
    protected abstract void onOutgoingCallEnded(Context ctx, String number, Date start, Date end);

    protected abstract void onMissedCall(Context ctx, String number, Date start);*/

    //Deals with actual events

    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    public void onCallStateChanged(Context context, int state, String number) {

        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if(lastState == state){
            //No change, debounce extras
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Date();
                savedNumber = number;
                //onIncomingCallReceived(context, number, callStartTime);
                if(preference.getString("lock_scren","").equals("on")){
                    if(preference.getString("count","").equals("1")){

                    }
                    else{
                        try{
                            LockScr.wm.removeView(LockScr.ll2);
                        }
                        catch (NullPointerException e){
                            context.stopService(new Intent(context,LockScr.class));
                        }

                        if(preference.getString("call_security","").equals("on")){
                            context.startService(new Intent(context,CallSecurityLockScr.class));

                            editor.putString("call_sec","1");
                            editor.commit();
                        }

                        editor.putString("count","1");
                        editor.putString("sound","off");
                        editor.commit();
                    }

                }


                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if(lastState != TelephonyManager.CALL_STATE_RINGING){
                    isIncoming = false;
                    callStartTime = new Date();
                    //onOutgoingCallStarted(context, savedNumber, callStartTime);
                }
                else
                {
                    isIncoming = true;
                    callStartTime = new Date();
                    //onIncomingCallAnswered(context, savedNumber, callStartTime);
                }

                break;
            case TelephonyManager.CALL_STATE_IDLE:
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if(lastState == TelephonyManager.CALL_STATE_RINGING){
                    //Ring but no pickup-  a miss
                    //onMissedCall(context, savedNumber, callStartTime);

                    if(preference.getString("lock_scren","").equals("on")){
                        if(preference.getString("LockScr","").equals("0")){

                        }
                        else{
                            if(preference.getString("count","").equals("1")){
                                int i;
                                i=Integer.parseInt(preference.getString("count",""));
                                i++;

                                editor.putString("count",String.valueOf(i));
                                editor.commit();

                                if (android.os.Build.VERSION.SDK_INT >= 23) {

                                    if (Settings.canDrawOverlays(context.getApplicationContext())) {
                                        context.startService(new Intent(context,LockScr.class));

                                        if(preference.getString("call_sec","").equals("0")){

                                        }
                                        else{
                                            if(preference.getString("call_security","").equals("on")){

                                                try{
                                                    CallSecurityLockScr.wm.removeView(CallSecurityLockScr.ll2);
                                                }
                                                catch (NullPointerException e){
                                                    context.stopService(new Intent(context,CallSecurityLockScr.class));

                                                }


                                            }

                                        }

                                        editor.putString("LockScr","1");
                                        editor.commit();
                                    }
                                }else{
                                    context.startService(new Intent(context,LockScr.class));

                                    if(preference.getString("call_sec","").equals("0")){

                                    }
                                    else{
                                        if(preference.getString("call_security","").equals("on")){
                                            try{
                                                CallSecurityLockScr.wm.removeView(CallSecurityLockScr.ll2);
                                            }
                                            catch (NullPointerException e){
                                                context.stopService(new Intent(context,CallSecurityLockScr.class));
                                            }

                                        }
                                    }

                                    editor.putString("LockScr","1");
                                    editor.commit();
                                }

                                // context.startService(new Intent(context,photo_lock.class));

                            }
                        }
                    }




                }
                else if(isIncoming){
                    //onIncomingCallEnded(context, savedNumber, callStartTime, new Date());
                    if(preference.getString("lock_scren","").equals("on")){
                        if(preference.getString("LockScr","").equals("0")){

                        }
                        else{
                            if(preference.getString("count","").equals("1")){
                                int i;
                                i=Integer.parseInt(preference.getString("count",""));
                                i++;

                                editor.putString("count",String.valueOf(i));
                                editor.commit();

                                if (android.os.Build.VERSION.SDK_INT >= 23) {

                                    if (Settings.canDrawOverlays(context.getApplicationContext())) {
                                        context.startService(new Intent(context,LockScr.class));

                                        if(preference.getString("call_sec","").equals("0")){

                                        }
                                        else{
                                            if(preference.getString("call_security","").equals("on")){

                                                try{
                                                    CallSecurityLockScr.wm.removeView(CallSecurityLockScr.ll2);
                                                }
                                                catch (NullPointerException e){
                                                    context.stopService(new Intent(context,CallSecurityLockScr.class));
                                                }
                                            }

                                        }

                                        editor.putString("LockScr","1");
                                        editor.commit();
                                    }
                                }else{
                                    context.startService(new Intent(context,LockScr.class));

                                    if(preference.getString("call_sec","").equals("0")){

                                    }
                                    else{
                                        if(preference.getString("call_security","").equals("on")){
                                            try{
                                                CallSecurityLockScr.wm.removeView(CallSecurityLockScr.ll2);
                                            }
                                            catch (NullPointerException e){
                                                context.stopService(new Intent(context,CallSecurityLockScr.class));
                                            }
                                        }
                                    }

                                    editor.putString("LockScr","1");
                                    editor.commit();
                                }

                                // context.startService(new Intent(context,photo_lock.class));

                            }
                        }
                    }
                }
                else{
                    //onOutgoingCallEnded(context, savedNumber, callStartTime, new Date());
                }
                break;
        }
        lastState = state;
    }
}