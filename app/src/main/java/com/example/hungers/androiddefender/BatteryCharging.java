package com.example.hungers.androiddefender;

import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;

import java.io.ByteArrayOutputStream;

import static android.R.attr.action;
import static android.content.Context.MODE_PRIVATE;

public class BatteryCharging extends BroadcastReceiver {
    public static String PREFS_NAME="hungers";
    public BatteryCharging() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String action = intent.getAction();
        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        if(action.equals(Intent.ACTION_POWER_CONNECTED))
        {
            Vibrator vib = (Vibrator) context.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vib.vibrate(50);
            editor.putString("charging","on");
            editor.commit();
            try{
                LockScr.tv.setVisibility(View.VISIBLE);
                LockScr.tv.setText("Charging");
            }
            catch(NullPointerException e){

            }

        }
        if(action.equals(Intent.ACTION_POWER_DISCONNECTED))
        {
            editor.putString("charging","off");
            editor.commit();
            try{
                LockScr.tv.setVisibility(View.INVISIBLE);
            }
            catch (NullPointerException e){

            }


        }

        if(action.equals(Intent.ACTION_BATTERY_OKAY))
        {
            editor.putString("charging","on");
            editor.commit();
            try{
                LockScr.tv.setVisibility(View.VISIBLE);
                LockScr.tv.setText("Full Charged");
            }
            catch (NullPointerException e){

            }


        }
        if(action.equals(Intent.ACTION_WALLPAPER_CHANGED))
        {
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(context.getApplicationContext());
            Drawable wallpaperDrawable = wallpaperManager.getDrawable();
            LockScr.ll2.setBackground(wallpaperDrawable);

            Bitmap yourSelectedImage = ((BitmapDrawable)wallpaperDrawable).getBitmap();

            yourSelectedImage = scaleCenterCrop(yourSelectedImage,dm.heightPixels,dm.widthPixels);


            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            editor.putString("image_data",encodedImage);
            editor.commit();
        }

    }
    public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }
}
