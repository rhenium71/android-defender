package com.example.hungers.androiddefender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class reboot_alert extends Activity {
    public static String PREFS_NAME="hungers";
    TextView tv,tv1;
    FrameLayout ll;
    Button b1,b2;
    Switch s1;
    LinearLayout ll2;
    CheckBox c1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams windowManager=getWindow().getAttributes();
        windowManager.dimAmount=0.60f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_reboot_alert);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();



        tv=(TextView) findViewById(R.id.tv1);
        tv1=(TextView) findViewById(R.id.tv_rs);


        b1=(Button) findViewById(R.id.btn_later);
        b2=(Button) findViewById(R.id.btn_star);

        ll =(FrameLayout) findViewById(R.id.fl1);



        ll2=(LinearLayout) findViewById(R.id.lila2);



        s1= new Switch(this);

        c1 = new CheckBox(this);



        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT ,10 );
        params3.setMargins(15, 0, 15, 0);

        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT ,70 );
        params4.setMargins(15, 0, 15, 0);


        s1.setLayoutParams(params3);
        s1.setText("Reboot Send SMS");
        s1.setTextColor(Color.rgb(0,166,255));
        c1.setLayoutParams(params4);
        c1.setText("Reboot Send SMS");
        c1.setTextColor(Color.rgb(0,166,255));


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        tv.setTypeface(type);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        b1.setTypeface(type1);
        b2.setTypeface(type1);


        c1.setTypeface(type1);

        s1.setTypeface(type1);


        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            ll2.addView(c1);



        } else{
            ll2.addView(s1);


        }



        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b1.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b1.getBackground().setAlpha(255);


                        break;
                }
                return false;
            }
        });

        b2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b2.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b2.getBackground().setAlpha(255);

                        finish();
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });

        //      reboot sms
        if(preference.getString("rebootSMS",null)==null){
            editor.putString("rebootSMS", "on");
            editor.commit();
        }

        if(preference.getString("rebootSMS",null).equals("off")){
            c1.setChecked(false);
        } else{
            c1.setChecked(true);
        }

        if(c1.isChecked()){
            editor.putString("rebootSMS", "on");
            editor.commit();
        }
        else{
            editor.putString("rebootSMS", "off");
            editor.commit();
        }

        c1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(!isChecked){

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot SMS OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("rebootSMS", "off");
                    editor.commit();
                }
                else{

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot SMS ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("rebootSMS", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////


//      reboot sms
        if(preference.getString("rebootSMS",null)==null){
            editor.putString("rebootSMS", "on");
            editor.commit();
        }

        if(preference.getString("rebootSMS",null).equals("off")){
            s1.setChecked(false);
        } else{
            s1.setChecked(true);
        }

        if(s1.isChecked()){
            editor.putString("rebootSMS", "on");
            editor.commit();
        }
        else{
            editor.putString("rebootSMS", "off");
            editor.commit();
        }

        s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(!isChecked){

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot SMS OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("rebootSMS", "off");
                    editor.commit();
                }
                else{

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot SMS ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("rebootSMS", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////


    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


    }
}
