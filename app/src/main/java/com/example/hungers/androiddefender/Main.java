package com.example.hungers.androiddefender;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import javax.crypto.spec.SecretKeySpec;

public class Main extends AppCompatActivity {

    ImageView lock_pic;
    Button reset,done;
    protected int count;
    protected byte[] codedBytes1,codedBytes2;
    protected final String[] preKeyNameX = {"pKey1X","pKey2X","pKey3X"},preKeyNameY = {"pKey1Y","pKey2Y","pKey3Y"};
    protected final String fileName = "Security_Key";
    protected Helper helper;
    protected SecretKeySpec sks;
    protected ImageView iv;
    protected RelativeLayout.LayoutParams params;
    protected RelativeLayout rl;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        count = 0;

        rl = (RelativeLayout) findViewById(R.id.relative_main);

        helper = new Helper();
        sks = helper.sksGenerator();

        lock_pic=(ImageView) findViewById(R.id.picture_lock);
        reset=(Button) findViewById(R.id.reset);
        done=(Button) findViewById(R.id.done);

        final SharedPreferences security = getSharedPreferences(fileName , Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = security.edit();
        editor.putString("sks" , Base64.encodeToString(sks.getEncoded(),Base64.DEFAULT));
        editor.commit();


        done.setEnabled(false);
        done.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        lock_pic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();

                byte[] decode = Base64.decode(security.getString("sks","0"),Base64.DEFAULT);
                SecretKeySpec secks = new SecretKeySpec(decode,0,decode.length,"AES");

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if(count < 3){
                            draw(x,y,rl);
                            codedBytes1 = helper.encodeData(x,secks);
                            codedBytes2 = helper.encodeData(y,secks);
                            editor.putString(preKeyNameX[count] , Base64.encodeToString(codedBytes1 , Base64.DEFAULT));
                            editor.commit();
                            editor.putString(preKeyNameY[count] , Base64.encodeToString(codedBytes2 , Base64.DEFAULT));
                            editor.commit();
                            count++;
                            if( count == 3){
                                done.setEnabled(true);
                                done.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View view, MotionEvent motionEvent) {
                                        startActivity(new Intent(getApplicationContext(),Confirmation.class));
                                        finish();
                                        return false;
                                    }
                                });
                                lock_pic.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View view, MotionEvent motionEvent) {
                                        return false;
                                    }
                                });
                            }
                        }
                        break;
                }
                return true;
            }
        });


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }

    protected void draw(int x, int y,RelativeLayout rl){
        iv = new ImageView(this);
        iv.setImageResource(R.drawable.circle5);
        params = new RelativeLayout.LayoutParams(100,100);
        params.setMargins(x-50,y-50,0,0);
        rl.addView(iv, params);
    }
}
