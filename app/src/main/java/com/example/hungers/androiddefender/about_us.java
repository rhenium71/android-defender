package com.example.hungers.androiddefender;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class about_us extends AppCompatActivity {

    public static String PREFS_NAME="hungers";
    TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15;
    ImageView iv1,iv2,iv3,iv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_about_us);

        t1=(TextView) findViewById(R.id.tv_a);
        t2=(TextView) findViewById(R.id.tv_d);
        t3=(TextView) findViewById(R.id.tv_t);
        t4=(TextView) findViewById(R.id.tv_r);
        t5=(TextView) findViewById(R.id.tv_d1);
        t6=(TextView) findViewById(R.id.tv_n);
        t7=(TextView) findViewById(R.id.tv_d2);
        t8=(TextView) findViewById(R.id.tv_e);
        t9=(TextView) findViewById(R.id.tv_d3);
        t10=(TextView) findViewById(R.id.tv_s);
        t11=(TextView) findViewById(R.id.tv_d4);
        t12=(TextView) findViewById(R.id.tv_e1);
        t13=(TextView) findViewById(R.id.tv_e2);
        t14=(TextView) findViewById(R.id.tv_e3);
        t15=(TextView) findViewById(R.id.tv_e4);

        iv1=(ImageView) findViewById(R.id.imageView1);
        iv2=(ImageView) findViewById(R.id.imageView2);
        iv3=(ImageView) findViewById(R.id.imageView3);
        iv4=(ImageView) findViewById(R.id.imageView4);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("iv","1");
                editor.commit();
                Intent i= new Intent(about_us.this,image_viewer.class);
                startActivity(i);
            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("iv","2");
                editor.commit();
                Intent i= new Intent(about_us.this,image_viewer.class);
                startActivity(i);
            }
        });
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("iv","3");
                editor.commit();
                Intent i= new Intent(about_us.this,image_viewer.class);
                startActivity(i);
            }
        });
        iv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("iv","4");
                editor.commit();
                Intent i= new Intent(about_us.this,image_viewer.class);
                startActivity(i);
            }
        });

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        t1.setTypeface(type);
        t3.setTypeface(type);
        t4.setTypeface(type);
        t6.setTypeface(type);
        t8.setTypeface(type);
        t10.setTypeface(type);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        t2.setTypeface(type1);
        t5.setTypeface(type1);
        t7.setTypeface(type1);
        t9.setTypeface(type1);
        t11.setTypeface(type1);

        Typeface type2 = Typeface.createFromAsset(getAssets(),"fonts/font11.ttf");
        t12.setTypeface(type2);
        t13.setTypeface(type2);
        t14.setTypeface(type2);
        t15.setTypeface(type2);




    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }

    }
}
